import br.com.conversor.Romano;
import org.junit.Assert;
import org.junit.Test;

public class RomanoTest {
    @Test
    public void validaRomanoTest(){
        Assert.assertEquals("I", new Romano().converterParaRomano(1));
        Assert.assertEquals("II", new Romano().converterParaRomano(2));
        Assert.assertEquals("V", new Romano().converterParaRomano(5));
        Assert.assertEquals("VIII", new Romano().converterParaRomano(8));
        Assert.assertEquals("X", new Romano().converterParaRomano(10));
        Assert.assertEquals("XX", new Romano().converterParaRomano(20));
        Assert.assertEquals("L", new Romano().converterParaRomano(50));
        Assert.assertEquals("C", new Romano().converterParaRomano(100));
        Assert.assertEquals("D", new Romano().converterParaRomano(500));
        Assert.assertEquals("M", new Romano().converterParaRomano(1000));
        Assert.assertEquals(null, new Romano().converterParaRomano(4000));
    }
}
