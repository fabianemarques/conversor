import br.com.conversor.Decimal;
import org.junit.Assert;
import org.junit.Test;

public class DecimalTest {
    @Test
    public void validaDecimalTest(){
        Assert.assertEquals(1, new Decimal().converterParaDecimal("I"));
        Assert.assertEquals(2, new Decimal().converterParaDecimal("II"));
        Assert.assertEquals(5, new Decimal().converterParaDecimal("V"));
        Assert.assertEquals(8, new Decimal().converterParaDecimal("VIII"));
        Assert.assertEquals(9, new Decimal().converterParaDecimal("IX"));
        Assert.assertEquals(10, new Decimal().converterParaDecimal("X"));
        Assert.assertEquals(20, new Decimal().converterParaDecimal("XX"));
        Assert.assertEquals(50, new Decimal().converterParaDecimal("L"));
        Assert.assertEquals(90, new Decimal().converterParaDecimal("XC"));
        Assert.assertEquals(100, new Decimal().converterParaDecimal("C"));
        Assert.assertEquals(500, new Decimal().converterParaDecimal("D"));
        Assert.assertEquals(1000, new Decimal().converterParaDecimal("M"));
        Assert.assertEquals(0, new Decimal().converterParaDecimal("Z"));
    }
}
