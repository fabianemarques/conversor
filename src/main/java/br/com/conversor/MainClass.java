package br.com.conversor;

import java.util.Scanner;
import br.com.conversor.Romano;

public class MainClass {

    static private Scanner scanner;

    static public void main(String[] args) {

        scanner = new Scanner(System.in);
        System.out.println("Digite a opção desejada:");
        System.out.println("1 - Para converter múmero decimal para romano");
        System.out.println("2 - Para converter múmero romano para decimal");
        int valorDigitado = scanner.nextInt();

        if (valorDigitado == 1) {
            scanner = new Scanner(System.in);
            System.out.println("Digite o valor: ");
            int valorDecimal = scanner.nextInt();

            Romano romano = new Romano();
            String retorno = romano.converterParaRomano(valorDecimal);
            if (retorno != null)
                System.out.println("Valor em Romano: " + retorno);

        } else if (valorDigitado == 2) {
            scanner = new Scanner(System.in);
            System.out.println("Digite o valor: ");
            String valorRomano = scanner.next();

            Decimal decimal = new Decimal();
            int retorno = decimal.converterParaDecimal(valorRomano);

            if (retorno > 0)
                System.out.println("Valor em Decimal: " + retorno);
        }
        else
            System.out.println("Opção inválida");
    }
}
