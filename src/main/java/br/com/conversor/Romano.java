package br.com.conversor;

import java.util.Map;
import java.util.TreeMap;

public class Romano {

    Map<Integer, String> mapRomano = new TreeMap<Integer, String>();

    public Romano() {

        mapRomano.put(1, "I");
        mapRomano.put(2, "II");
        mapRomano.put(3, "III");
        mapRomano.put(4, "IV");
        mapRomano.put(5, "V");
        mapRomano.put(9, "IX");
        mapRomano.put(10, "X");
        mapRomano.put(40, "XL");
        mapRomano.put(50, "L");
        mapRomano.put(90, "XC");
        mapRomano.put(100, "C");
        mapRomano.put(400, "CD");
        mapRomano.put(500, "D");
        mapRomano.put(900, "CM");
        mapRomano.put(1000, "M");
    }

    public String converterParaRomano(int num ){
        if (num < 1||num >=3000  ) {
            System.out.println("Valor inválido");
            return null;
        }

        int tam = String.valueOf(num).length();

        String romano = "";

        while (num > 0) {
            int maior = 1;
            for (Integer i : mapRomano.keySet()) {
                if (num - i > -1) {
                    maior = i;
                }
            }
            num -= maior;
            romano += mapRomano.get(maior);
        }

        return romano;
    }

 }
