package br.com.conversor;

import java.util.HashMap;
import java.util.Map;

public class Decimal {

    private static Map<String, Integer> mapDecimal = new HashMap<String, Integer>();

    public Decimal() {
        mapDecimal.put("M", 1000);
        mapDecimal.put("CM", 900);
        mapDecimal.put("D", 500);
        mapDecimal.put("C", 100);
        mapDecimal.put("XC", 90);
        mapDecimal.put("L", 50);
        mapDecimal.put("X", 10);
        mapDecimal.put("IX", 9);
        mapDecimal.put("V", 5);
        mapDecimal.put("I", 1);
    }

    public int converterParaDecimal(String romano) {
        int tam = romano.length();
        int ret = 0;
        int retornoAnt = 0;
        while (tam > 0) {
            tam--;

            if (!mapDecimal.containsKey(String.valueOf(romano.charAt(tam)))){
                System.out.println("Valor inválido");
                return 0;
            }

            int retorno = mapDecimal.get(String.valueOf(romano.charAt(tam)));

            if (retornoAnt > retorno) ret -= retorno;
            else ret += retorno;

            retornoAnt = retorno;
        }

        return ret;
    }
}